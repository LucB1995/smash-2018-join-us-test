# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This respository contains an alpha port of the Join Us Website for Drupal 8

### How do I get set up? ###

Requirements:
 - PHP 5
 - Apache 2.4
 - MySQL

Installation (For a Local Copy):
 
 1. Copy Source Files into a folder that can be accessed through localhost
 2. Create a database on MySQL and run mysql.sql (located in root directory)
 	- Create Database: mysql > Create Database name_of_database;
	- Run mysql.sql: mysql > source destination/of/mysql.sql
 3. Access Web Application through http://localhost/name_of_folder
 4. During Database Configuration, use the name of the database create in step 2
 5. Complete Drupal Installation

### Changes from Original Drupal 7 Installation ###
 - Pages & Panels module are not being used to structure the appearance of pages, instead Structure > Block Layout is being used to configure this
 - There is no Registration Module, so we are using RNG Instead

### Modules Used ###
 - Chaos Tools (https://ftp.drupal.org/files/projects/ctools-8.x-3.0.tar.gz)
 - RNG (https://ftp.drupal.org/files/projects/rng-8.x-1.5.tar.gz)
 - RNG Quick (https://ftp.drupal.org/files/projects/rng_quick-8.x-1.0.tar.gz)
 - RNG Contact (https://ftp.drupal.org/files/projects/rng_contact-8.x-1.0-beta3.tar.gz)
 - Markdown (https://ftp.drupal.org/files/projects/markdown-8.x-1.2.tar.gz)
 - Dynamic Entity Reference (https://ftp.drupal.org/files/projects/dynamic_entity_reference-8.x-1.4.tar.gz)
 - Courier (https://ftp.drupal.org/files/projects/courier-8.x-1.0-alpha9.tar.gz)
 - Unlimited Number (https://ftp.drupal.org/files/projects/unlimited_number-8.x-1.0-beta2.tar.gz)
 - Veoa (https://ftp.drupal.org/files/projects/veoa-8.x-1.0.tar.gz)
 - Views Advanced Routing (https://ftp.drupal.org/files/projects/views_advanced_routing-8.x-1.0-rc2.tar.gz)
 - Unique Field (https://ftp.drupal.org/files/projects/unique_field-8.x-2.x-dev.tar.gz)

### Work that needs to be done to acheive beta status ###

There are still some aspects of the alpha port of the Join Us Website that need to be worked on to acheive beta status:
 - Configuring the View for All Registrations
 We may need to change the design of this to display a view of individual departments instead of all departments as the fields used to display registration fields such as full name, and resume files are different across each registration forms, therefore we cannot have a table with All registrations as the no fields are shared. I'm looking for a fix for this.
 
- Redirection on Department Views
The Department Views on the top of the page needs to redirect to /select-role/department_name in pages with positions and /all-registrations/department_name when selected instead of /department/department_name

- Apply Now Button on Teasers for positions
Need to figure out a way to put a button that takes the user to the registration form for the position. The method used in Drupal 7 in the registrations module does not apply for Drupal 8 in the RNG Module.

- Creating Registration Forms
At the moment only a few registration forms have been made. I have also combined some existing forms into "Generic" & "Generic with Portfolio" as the fields were the same across different forms

- Theming
There is no theme at the moment. Current Design is dry as fuck.

- Markdown
The Markdown module doesn't seem to be working, this will need to be fixed, or we could always tell people to use HTML instead.

### Who do I talk to? ###

Any Questions should be redirected to Brian Luc (brian.luc@smash.org.au).